#!/usr/bin/env python
# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import sys, os

import gobject
gobject.threads_init()

# import pygst
# pygst.require('0.10')
import gst

class TestPipeline(gst.Pipeline):
    def __init__(self):
        gst.Pipeline.__init__(self, 'TestPipeline')
        self._mute = False
        self._playing = False
        self._rate = 1.0
        
        # Create elements
        src = gst.element_factory_make("videotestsrc", "src")
        q = gst.element_factory_make("queue", "q")
        capsfilt = gst.Caps("video/x-raw-rgb, framerate=(fraction)1/4")
        self._sink = gst.element_factory_make("flugdlsink", "sink")
        # Add elements to bin
        self.add(src, q, self._sink)
        # Link the elements
        src.link(q)
        q.link(self._sink, capsfilt)
        qsrcpad = q.get_pad("src")
        qsrcpad.add_buffer_probe(self.buffer_probe)

    def buffer_probe(self, pad, buffer):
        buffer.duration = buffer.duration / 4
        return True
            
    def play(self):
        self._playing = True
        self.set_state(gst.STATE_PLAYING)

    def pause(self):
        self._playing = False
        self.set_state(gst.STATE_PAUSED)

    def toggle_mute(self):
        self._mute = not self._mute
        self._sink.set_property('mute', self._mute)

    def toggle_play_pause(self):
        if self._playing:            
            self.pause()
        else:            
            self.play()

    def toggle_rate(self):
        if self._rate == 1.0:
            self._rate = 2.0
        else:
            self._rate = 1.0

        try:
            position, format = self.query_position(gst.FORMAT_TIME)
        except:
            position = gst.CLOCK_TIME_NONE
            
        event = gst.event_new_seek(self._rate, gst.FORMAT_TIME,
            gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_KEY_UNIT,
            gst.SEEK_TYPE_SET, position,
            gst.SEEK_TYPE_SET, -1)
        res = self.send_event(event)
        if res:
            print 'rate set to %s' % self._rate
        else:
            print 'change rate failed'

class Test():
    def __init__(self):

        self.pipeline = TestPipeline()
        
        def on_eos():
            self.quit()

        self.pipeline.on_eos = lambda *x: on_eos()
        # The MainLoop
        self.mainloop = gobject.MainLoop()
        gobject.io_add_watch(sys.stdin, gobject.IO_IN, self.on_stdin)

        self.pipeline.play()
        try:
            self.mainloop.run()
        except KeyboardInterrupt:
            self.quit()

    def on_stdin(self, fd, condition):
        c = os.read(fd.fileno(), 1)
        
        if c == "q":
            self.quit()
        elif c == "m":
            self.pipeline.toggle_mute()
        elif c == "p":
            self.pipeline.toggle_play_pause()
        elif c == "r":
            self.pipeline.toggle_rate()
            
        return True
    
    def quit(self):
        self.mainloop.quit()

                                                              
def main(args):

    t = Test()

if __name__ == '__main__':
    sys.exit(main(sys.argv))
