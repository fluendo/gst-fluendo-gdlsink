#!/usr/bin/env python
# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import sys, os

import gobject
gobject.threads_init()

# import pygst
# pygst.require('0.10')
import gst

# The Custom Video Bin
class CustomVideoBin(gst.Bin):
    def __init__(self, desktop, image):
        gst.Bin.__init__(self, 'CustomVideoBin')

        if desktop:
            queue = gst.element_factory_make('queue', 'vqueue')
            self.add(queue)

            csconvert = gst.element_factory_make('ffmpegcolorspace')
            self.add(csconvert)

            if image is not None:
                video_sink = gst.element_factory_make('pgmimagesink')
                video_sink.set_property('image', image)
            else:
                video_sink = gst.element_factory_make('xvimagesink')
            self.add(video_sink)

            gst.element_link_many(queue, csconvert, video_sink)
            sink = queue.get_pad('sink')
        else:
            vidpproc = gst.element_factory_make('ismd_vidpproc')
            self.add(vidpproc)

            video_sink = gst.element_factory_make('ismd_vidrend_sink')
            self.add(video_sink)

            gst.element_link_many(vidpproc, video_sink)
            sink = vidpproc.get_pad('sink')

        self.add_pad(gst.GhostPad('sink', sink))

# The Custom Audio Bin
class CustomAudioBin(gst.Bin):
    def __init__(self, desktop, conv = False):
        gst.Bin.__init__(self, 'CustomAudioBin')

        self._is_desktop = desktop
        queue = gst.element_factory_make('queue', 'aqueue')
        self.add(queue)

        if conv or desktop:
          aconvert = gst.element_factory_make('audioconvert')
          self.add(aconvert)

        if desktop:
          self._volume = gst.element_factory_make('volume')
          self.add(self._volume)

          self._audio_sink = gst.element_factory_make('alsasink')
          self.add(self._audio_sink)
          gst.element_link_many(queue, aconvert, self._volume, self._audio_sink)
        else:
          self._audio_sink = gst.element_factory_make('ismd_audio_sink')
          self.add(self._audio_sink)

          if conv:
              gst.element_link_many(queue, aconvert, self._audio_sink)
          else:
              gst.element_link_many(queue, self._audio_sink)

        self.add_pad(gst.GhostPad('sink', queue.get_pad('sink')))

    def set_volume(self, volume):
        gst.info("volume %f" % volume)
        if self._is_desktop:
            element = self._volume
        else:
            element = self._audio_sink
            if volume == 0.0:
                element.set_property('mute', True)
            elif volume > 1.0:
                element.set_property('mute', False)
            else:
                element.set_property('mute', False)

        element.set_property('volume', volume)

    def toggle_mute(self):
        mute = self._audio_sink.get_property('mute')
        self._audio_sink.set_property('mute', not mute)

# The Custom Subtitle Bin
class CustomSubBin(gst.Bin):
    def __init__(self, desktop):
        gst.Bin.__init__(self, 'CustomSubBin')
        self._gdlsink = None

        # sq0 = gst.element_factory_make('queue', 'sq0')
        # self.add(sq0)

        xsubparse = gst.element_factory_make('fluxsubparse', 'xsubparse')
        self.add(xsubparse)

        #sq1 = gst.element_factory_make('queue', 'sq1')
        #self.add(sq1)

        if desktop:
            csconvert = gst.element_factory_make('ffmpegcolorspace')
            self.add(csconvert)

            sub_sink = gst.element_factory_make('ximagesink')
            self.add(sub_sink)

            gst.element_link_many(xsubparse, csconvert, sub_sink)
        else:
            sub_sink = gst.element_factory_make('flugdlsink')
            self._gdlsink = sub_sink
            self.add(sub_sink)

            gst.element_link_many(xsubparse, sub_sink)

        sink = xsubparse.get_pad('sink')
        self.add_pad(gst.GhostPad('sink', sink))

    def mute(self):
        if self._gdlsink is not None:
            self._gdlsink.set_property('mute', True)

    def unmute(self):
        if self._gdlsink is not None:
            self._gdlsink.set_property('mute', False)

class CustomPlayBin(gst.Pipeline):
    __gproperties__ = {
        'source': (gst.Element, "source", "Source element", gobject.PARAM_READABLE)
    }

    def __init__(self):
        gst.Pipeline.__init__(self, 'CustomPlayBin')

        self.volume = 1.0
        self.source = None
        self._decodebin = None
        self._videobin = None
        self._audiobin = None
        self._subbin = None
        self._subselector = None
        self._subpads = []
        self._clock_provider = None
        self._uri = ''
        self._image = None

        # Detect if we are running in desktop or in the canmore
        registry = gst.registry_get_default()
        feature = registry.lookup_feature('ismd_audio_sink')
        if feature != None:
            self._is_desktop = False
        else:
            self._is_desktop = True

    def seek(self, rate, format, flags, cur_type, cur, stop_type, stop):
        # old_state = self.get_state(0)[1]
        # if old_state == gst.STATE_PLAYING:
        #    gst.info('old_state is %s, going to paused' % old_state)
        #    self.set_state(gst.STATE_PAUSED)
        #    current = self.get_state(0)[1]

        if self._videobin is not None:
            gst.info("doing seek in videobin")
            ret = self._videobin.seek(rate, format, flags, cur_type, cur, stop_type, stop)
        elif self._audiobin is not None:
            gst.info("doing seek in audiobin")
            ret = self._audiobin.seek(rate, format, flags, cur_type, cur, stop_type, stop)
        else:
            ret = False

        # gst.info('changing back to old_state')
        # self.set_state(old_state)

        return ret

    def query_position(self, qformat):
        # old_state = self.get_state(0)[1]
        # if old_state == gst.STATE_PLAYING:
        #    gst.info('old_state is %s, going to paused' % old_state)
        #    self.set_state(gst.STATE_PAUSED)
        #    current = self.get_state(0)[1]

        if self._videobin is not None:
            gst.info("doing query_position in videobin")
            try:
                position, format = self._videobin.query_position(qformat)
            except:
                position = gst.CLOCK_TIME_NONE
                format = gst.FORMAT_UNDEFINED
        elif self._audiobin is not None:
            gst.info("doing query_position in audiobin")
            try:
                position, format = self._audiobin.query_position(qformat)
            except:
                position = gst.CLOCK_TIME_NONE
                format = gst.FORMAT_UNDEFINED

        # gst.info('changing back to old_state')
        # self.set_state(old_state)
        return (position, format)

    def add_videobin(self):
        if self._videobin is None:
            self._videobin = CustomVideoBin(self._is_desktop, self._image)
            gst.info('add videobin %s' % self._videobin)
            self.add (self._videobin)
            self._videobin.sync_state_with_parent()

    def link_videobin(self, srcpad):
        if self._videobin is not None:
            gst.info('link videobin to %s' % srcpad)
            sinkpad = self._videobin.get_pad('sink')
            srcpad.link (sinkpad)

    def remove_videobin(self):
        if self._videobin is not None:
            sinkpad = self._videobin.get_pad('sink')
            peer = sinkpad.get_peer()
            if peer is not None:
                gst.info('unlink videobin pad %s' % peer)
                peer.unlink (sinkpad)

            gst.info('remove videobin %s' % self._videobin)
            self._videobin.set_state(gst.STATE_NULL)
            self.remove (self._videobin)
            del self._videobin
            self._videobin = None

    def add_audiobin(self, volume, conv = False):
        if self._audiobin is None:
            self._audiobin = CustomAudioBin(self._is_desktop, conv)
            self._audiobin.set_volume(volume)
            gst.info('add audiobin %s' % self._audiobin)
            self.add (self._audiobin)
            self._audiobin.sync_state_with_parent()

    def link_audiobin(self, srcpad):
        if self._audiobin is not None:
            gst.info('link audiobin to %s' % srcpad)
            sinkpad = self._audiobin.get_pad('sink')
            srcpad.link (sinkpad)

    def remove_audiobin(self):
        if self._audiobin is not None:
            sinkpad = self._audiobin.get_pad('sink')
            peer = sinkpad.get_peer()
            if peer is not None:
                gst.info('unlink audiobin pad %s' % peer)
                peer.unlink (sinkpad)

            gst.info('remove audiobin %s' % self._audiobin)
            self._audiobin.set_state(gst.STATE_NULL)
            self.remove (self._audiobin)
            del self._audiobin
            self._audiobin = None

    def add_subbin(self):
        if self._subbin is None:
            self._subbin = CustomSubBin(self._is_desktop)
            gst.info('add subbin %s' % self._subbin)
            self.add (self._subbin)
            self._subbin.sync_state_with_parent()

    def link_subbin(self, srcpad):
        if self._subbin is not None:
            gst.info('link subbin to %s' % srcpad)
            sinkpad = self._subbin.get_pad('sink')
            srcpad.link (sinkpad)

    def remove_subbin(self):
        if self._subbin is not None:
            sinkpad = self._subbin.get_pad('sink')
            peer = sinkpad.get_peer()
            if peer is not None:
                gst.info('unlink subbin pad %s' % peer)
                peer.unlink (sinkpad)

            gst.info('remove subbin %s' % self._subbin)
            self._subbin.set_state(gst.STATE_NULL)
            self.remove (self._subbin)
            del self._subbin
            self._subbin = None

    def add_clock_provider(self):
        if self._clock_provider is None:
            gst.info("adding clock provider")
            self._clock_provider = gst.element_factory_make('ismd_clock_provider')
            self.add(self._clock_provider)
            self._clock_provider.sync_state_with_parent()

    def remove_clock_provider(self):
        if self._clock_provider is not None:
            gst.info("removing clock provider")
            self._clock_provider.set_state(gst.STATE_NULL)
            self.remove (self._clock_provider)
            del self._clock_provider
            self._clock_provider = None

    def add_decodebin(self, uri):
        if self._decodebin is None:
            gst.info("adding uridecodebin")
            self._decodebin = gst.element_factory_make('uridecodebin')
            gst.info("connecting handlers")
            # Connect handler for 'pad-added' signal
            self._decodebin.connect('pad-added', self.on_new_decoded_pad)
            self._decodebin.connect('notify::source', self.on_notify_source)
            # Connect handler for 'autoplug-continue' signal in Canmore
            self._decodebin.connect('autoplug-continue', self.on_autoplug_continue)
            self._decodebin.connect('autoplug-select', self.on_autoplug_select)

            gst.info("setting uri %s" % uri)
            self._decodebin.set_property('uri', uri)
            gst.info("adding uridecodebin to pipeline")
            self.add(self._decodebin)
            gst.info("sync state with the pipeline")
            self._decodebin.sync_state_with_parent()

    def remove_decodebin(self):
        if self._decodebin is not None:
            gst.info("removing uridecodebin")
            self._decodebin.set_state(gst.STATE_NULL)
            self.remove (self._decodebin)
            del self._decodebin
            self._decodebin = None

    def add_subselector(self, pad):
        # FIXME: add a bin
        if self._subselector is None:
            gst.info("adding fluendo subtitle selector")
            self._subselector = gst.element_factory_make('flusubtitle-selector')
            self.add(self._subselector)
            gst.info("sync state with the pipeline")
            self._subselector.sync_state_with_parent()
            srcpad = self._subselector.get_pad('src')

            # add subtitle rendering bin
            self.add_subbin()
            self.link_subbin(srcpad)

        queue = gst.element_factory_make('queue')
        self.add(queue)
        qpad = queue.get_pad('src')
        spad = self._subselector.get_request_pad('sink%d')
        qpad.link(spad)
        pad.link(queue.get_pad('sink'))
        queue.sync_state_with_parent()
        self._subpads.append(spad)
        print('plugged a subtitle!')

    def remove_subselector (self):
        if self._subselector is not None:
            self.remove_subbin()
            gst.info("removing subselector")
            self._subselector.set_state(gst.STATE_NULL)
            self.remove (self._subselector)
            del self._subselector
            self._subselector = None


    def do_cleanup(self):
        current = self.get_state(0)[1]
        if current != gst.STATE_NULL:
            gst.info('do_cleanup')
            self.set_state(gst.STATE_READY)
            current = self.get_state(0)[1]
            gst.info('changed state to %s' % current)
            self.remove_videobin()
            self.remove_audiobin()
            self.remove_decodebin()
            self.remove_subselector()
            self.remove_clock_provider()
            self._uri = ""
            self.set_state(gst.STATE_NULL)
            current = self.get_state(0)[1]
            gst.info('changed state to %s' % current)

    def set_uri(self, uri):
        suri = str(uri)
        if suri != self._uri:
            current = self.get_state(0)[1]
            self.do_cleanup()
            if not self._is_desktop:
                self.add_clock_provider()
            self.add_decodebin(uri)
            gst.info('going to change state to %s' % current)
            self.set_state(current)
            self._uri = suri

    def get_uri(self):
        if self._decodebin is not None:
            return self._decodebin.get_property('uri')
        else:
            return None

    def get_property(self, property):
        if property == 'stream-info-value-array':
            return []
        elif property == 'volume':
            return self.volume
        elif property == 'uri':
            return self.get_uri()
        elif property == 'source':
            return self.source
        else:
            return None

    def set_property(self, property, value):
        if property == 'uri':
            self.set_uri (value)
        elif property == 'volume':
            self.volume = value
            if self._audiobin is not None:
                self._audiobin.set_volume(value)

    def on_notify_source(self, element, pad):
        if self.source is not None:
            del self.source
            self.source = None
        self.source = self._decodebin.get_property('source')
        self.notify('source')

    def on_new_decoded_pad(self, element, pad):
        padname = pad.get_name()
        caps = pad.get_caps()
        name = caps[0].get_name()
        gst.info('padname %s %s' % (padname, caps))
        if 'video/x-raw' in name:
            if self._videobin is None: # Only link once
                self.add_videobin()
                self.link_videobin(pad)
        elif name in ('audio/x-raw-int', 'audio/x-raw-float',
                      'audio/mpeg', 'audio/x-mpeg',
                      'audio/x-ac3', 'audio/x-dd', 'audio/x-private1-ac3',
                      'audio/x-private1-lpcm', 'audio/x-aac', 'audio/x-ddplus',
                      'audio/x-private1-dts', 'audio/x-dts'):
            if self._audiobin is None: # Only link once
                conv = name in ('audio/x-raw-float')
                self.add_audiobin(self.volume, conv)
                self.link_audiobin(pad)
        elif 'application/x-xsub' in name:
            self.add_subselector(pad)

    def on_autoplug_continue(self, element, pad, caps):
        pcaps = pad.get_caps()
        stru = pcaps[0]
        name = stru.get_name()
        gst.info('on_autoplug_continue called with: %s' % caps)

        if name == 'application/x-xsub':
            return False
        elif self._is_desktop:
            return True

        if name == 'audio/mpeg':
            if stru.has_field('parsed') and stru['parsed'] == True:
                return False
            elif stru.has_field('framed') and stru['framed'] == True:
                return False
            elif stru.has_field('mpegversion') and stru['mpegversion'] == 4:
                return False
            else:
                return True
        elif name in ('audio/x-mpeg', 'audio/x-ac3', 'audio/x-dd',
                      'audio/x-private1-ac3', 'audio/x-private1-lpcm',
                      'audio/x-aac', 'audio/x-ddplus'):
            return False
        else:
            return True

    def on_autoplug_select(self, decodebin, pad, caps, factory):
        # we can quickly filter on some factory names
        if factory.get_name() == 'avidemux':
            self.debug('detected avidemux demuxer %s, skipping' \
                    % factory.get_name())
            return 2 # AUTOPLUG_SELECT_SKIP

        if self._is_desktop:
            return 0 # AUTOPLUG_SELECT_TRY

        if factory.get_name() == 'flump3dec':
            self.debug('detected fluendo mp3 decoder %s, skipping' \
                    % factory.get_name())
            return 2 # AUTOPLUG_SELECT_SKIP

        return 0 # AUTOPLUG_SELECT_TRY

    def set_drawable(self, value):
        self._image = value

    def next_subtitle(self):
        pad = self._subselector.get_property('active-pad')
        gst.info("currently active pad name %s" % pad.get_name())
        cur_sub = self._subpads.index(pad)
        gst.info("currently selected pad %d" % cur_sub)
        if cur_sub == len(self._subpads)-1:
            cur_sub = 0
        else:
            cur_sub = cur_sub + 1
        self._subselector.set_property('active-pad', self._subpads[cur_sub])
        self._subbin.mute()
        self._subbin.unmute()

class GstPlayer:
    STOPPED = 0
    PLAYING = 1
    PAUSED = 2
    BUFFERING = 3
    PREROLLING = 4

    def __init__(self):
        self.playing = False
        self.on_eos = False
        self._cbuffering = -1
        self.status = self.STOPPED
        self.target_status = self.STOPPED
        self._rate = 1.0

        self.player = CustomPlayBin()
        # self.player.connect('notify::source', self.on_source_created)

        bus = self.player.get_bus()
        # bus.enable_sync_message_emission()
        # bus.connect('message', self.on_message)
        bus.connect('message::eos', self.on_message_eos)
        bus.connect('message::error', self.on_message_error)
        bus.connect('message::state-changed', self.on_message_state_changed)
        bus.connect('message::buffering', self.on_message_buffering)
        bus.connect('message::clock-lost', self.on_message_clock_lost)
        bus.add_signal_watch()

    def on_source_created(self, pipeline, *args):
        source = self.pipeline.get_property('source')
        try:
            source.set_property("user-agent", "quicktime")
        except:
            gst.warning('Source element %s doesn t have property %s' % \
                    (source, key))

    def on_message(self, bus, message):
        t = message.type
        if t == gst.MESSAGE_ERROR:
            err, debug = message.parse_error()
            gst.info("Error: %s" % err, debug)
            if self.on_eos:
                self.on_eos()
            self.playing = False
        elif t == gst.MESSAGE_EOS:
            if self.on_eos:
                self.on_eos()
            self.playing = False
        elif t == gst.MESSAGE_NEW_CLOCK:
            gst.info("new clock")
            self.player.set_state(gst.STATE_PAUSED)
            self.player.set_state(gst.STATE_PLAYING)
        else:
            gst.info("received message: %s" % message)

    def on_message_eos(self, bus, message):
        if self.on_eos:
            self.on_eos()
        self.playing = False

    def on_message_error(self, bus, message):
        err, msg = message.parse_error()
        code = message.structure['gerror'].code
        self.player.do_cleanup()
        self.status = self.STOPPED
        self.target_status = self.status
        gst.error("Gstreamer %s:%s" % (err, msg))

    def on_message_state_changed(self, bus, message):
        if message.src != self.player:
            return

        old_state, new_state, pending = message.parse_state_changed()
        gst.info ("old %s current %s pending %s status %s target status %s" % \
            (old_state, new_state, pending, self.status, self.target_status))
        if new_state == gst.STATE_PLAYING:
            if self.status != self.PLAYING:
                self.status = self.PLAYING
                self.playing = True

        elif new_state == gst.STATE_PAUSED:
            if self.status != self.BUFFERING:
                if self.target_status == self.PLAYING:
                    self.player.set_state(gst.STATE_PLAYING)
                else:
                    self.status = self.PAUSED

    def on_message_buffering(self, bus, message):
        percent = message.parse_buffering()

        if math.floor(percent/5) > self._cbuffering:
            self._cbuffering = math.floor(percent/5)
            gst.info("buffering %s" % percent)

        if percent == 100:
            if self.target_status == self.PLAYING:
                gst.info("buffering done going to play")
                self.status = self.target_status
                self.player.set_state(gst.STATE_PLAYING)
            self._cbuffering = -1
        elif self.status != self.BUFFERING:
            if self.status == self.PLAYING:
                self.player.set_state(gst.STATE_PAUSED)

            self.status = self.BUFFERING

    def on_message_clock_lost(self, bus, message):
        self.player.set_state(gst.STATE_PAUSED)
        self.player.set_state(gst.STATE_PLAYING)

    def set_location(self, location):
        self.player.set_property('uri', location)

    def query_position(self):
        "Returns a (position, duration) tuple"
        try:
            position, format = self.player.query_position(gst.FORMAT_TIME)
        except:
            position = gst.CLOCK_TIME_NONE

        try:
            duration, format = self.player.query_duration(gst.FORMAT_TIME)
        except:
            duration = gst.CLOCK_TIME_NONE

        return (position, duration)

    def seek(self, location):
        """
        @param location: time to seek to, in nanoseconds
        """
        gst.debug("seeking to %r" % location)

        res = self.player.seek (self._rate, gst.FORMAT_TIME,
            gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_KEY_UNIT,
            gst.SEEK_TYPE_SET, location,
            gst.SEEK_TYPE_NONE, 0)

        if res:
            gst.info("setting new stream time to 0")
            self.player.set_new_stream_time(0L)
        else:
            gst.error("seek to %r failed" % location)

    def pause(self):
        gst.info("pausing player")
        self.target_status = self.PAUSED
        self.player.set_state(gst.STATE_PAUSED)
        self.playing = False

    def play(self):
        gst.info("playing player")
        self.target_status = self.PLAYING
        current = self.player.get_state(0)[1]
        if current == gst.STATE_PAUSED:
            self.player.set_state(gst.STATE_PLAYING)
        elif current != gst.STATE_PLAYING :
            self.player.set_state(gst.STATE_PAUSED)
            self.status = self.PREROLLING

    def stop(self):
        self.player.do_cleanup()
        del self.player
        self.player = None
        if self.status != self.STOPPED:
            self.status = self.STOPPED
            self.target_status = self.status
        gst.info("stopped player")

    def get_state(self, timeout=1):
        return self.player.get_state(timeout=timeout)

    def is_playing(self):
        return self.playing

    def set_rate(self, rate):
        self._rate = rate
        try:
            position, format = self.player.query_position(gst.FORMAT_TIME)
        except:
            position = gst.CLOCK_TIME_NONE

        res = self.player.seek(self._rate, gst.FORMAT_TIME,
            gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_KEY_UNIT,
            gst.SEEK_TYPE_SET, position,
            gst.SEEK_TYPE_SET, -1)

        if res:
            gst.info("rate set to %s" % rate)
        else:
            gst.warining("change rate failed")

    def next_subtitle(self):
        self.player.next_subtitle()


class PlayerTUI():
    def __init__(self, location):

        self.player = GstPlayer()

        self.duration = gst.CLOCK_TIME_NONE
        self.pos = 0

        def on_eos():
            self.quit()

        print "q: quit"
        print "p: show position"
        print "f: seek forward"
        print "b: seek backward"
        print "s: pause"
        print "r: resume playback (play)"
        print "h: half speed"
        print "1: normal rate"
        print "2: 2x playback"
        print "4: 4x playback"
        print "n: next subtitle"

        self.player.on_eos = lambda *x: on_eos()
        # The MainLoop
        self.mainloop = gobject.MainLoop()
        gobject.io_add_watch(sys.stdin, gobject.IO_IN, self.on_stdin)

        self.player.set_location(location)
        self.player.play()
        try:
            self.mainloop.run()
        except KeyboardInterrupt:
            self.quit()

    def on_stdin(self, fd, condition):
        c = os.read(fd.fileno(), 1)

        if c == "q":
            self.quit()
        elif c == "p":
            self.show_position()
        elif c == "f":
            self.seek_forward()
        elif c == "b":
            self.seek_backward()
        elif c == "s":
            self.player.pause()
        elif c == "r":
            self.player.play()
        elif c == "h":
            self.player.set_rate(0.5)
        elif c == "1":
            self.player.set_rate(1.0)
        elif c == "2":
            self.player.set_rate(2.0)
        elif c == "3":
            self.player.set_rate(3.0)
        elif c == "n":
            self.player.next_subtitle()

        return True

    def quit(self):
        self.player.stop()
        gst.info("finished cleanup")
        del self.player
        self.player = None
        self.mainloop.quit()

    def show_position(self):
        position, duration = self.player.query_position()
        print "%s / %s" % (gst.TIME_ARGS(position), gst.TIME_ARGS(duration))

    def seek_forward(self):
        c_position, c_duration = self.player.query_position()
        if self.duration == gst.CLOCK_TIME_NONE:
            self.duration = c_duration
        self.pos = min (self.pos + 10, 90)
        n_position = self.duration * self.pos / 100
        print "-> %s ( %d )" % (gst.TIME_ARGS(n_position), self.pos)
        self.player.seek (n_position)

    def seek_backward(self):
        c_position, c_duration = self.player.query_position()
        if self.duration == gst.CLOCK_TIME_NONE:
            self.duration = c_duration
        self.pos = max (self.pos - 10, 0)
        n_position = self.duration * self.pos / 100
        print "-> %s ( %d )" % (gst.TIME_ARGS(n_position), self.pos)
        self.player.seek (n_position)

def main(args):
    def usage():
        sys.stderr.write("usage: %s URI-OF-MEDIA-FILE\n" % args[0])
        sys.exit(1)

    if len(args) != 2:
        usage()

    if not gst.uri_is_valid(args[1]):
        sys.stderr.write("Error: Invalid URI: %s\n" % args[1])
        sys.exit(1)

    tui = PlayerTUI(args[1])

if __name__ == '__main__':
    sys.exit(main(sys.argv))
