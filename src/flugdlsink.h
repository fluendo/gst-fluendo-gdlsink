/*
    This file is provided under a dual BSD/LGPLv2.1 license.  When using
    or redistributing this file, you may do so under either license.

    LGPL LICENSE SUMMARY

    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of the
    License.

    This library is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA. The full GNU Lesser General Public License is included in this
    distribution in the file called LICENSE.LGPL.
    
    Contat Information for Fluendo:
        FLUENDO S.A.
        World Trade Center Ed Norte 4 pl.
        Moll de Barcelona
        08039 BARCELONA - SPAIN

    BSD LICENSE

    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

      - Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      - Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in
        the documentation and/or other materials provided with the
        distribution.
      - Neither the name of FLUENDO S.A. nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef __GST_FLU_GDL_SINK_H__
#define __GST_FLU_GDL_SINK_H__

#include <gst/video/video.h>
#include <gst/video/gstvideosink.h>

#include <libgdl.h>
#include <string.h>

#define NUM_SURFACES 2

#define IS_IAP_PLANE(plane) \
    (plane == GDL_PLANE_ID_IAP_A || plane == GDL_PLANE_ID_IAP_B)

G_BEGIN_DECLS
#define GST_TYPE_FLUGDLSINK \
  (gst_flugdlsink_get_type())
#define GST_FLUGDLSINK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj), GST_TYPE_FLUGDLSINK, GstFluGdlSink))
#define GST_FLUGDLSINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass), GST_TYPE_FLUGDLSINK, GstFluGdlSinkClass))
#define GST_IS_FLUGDLSINK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj), GST_TYPE_FLUGDLSINK))
#define GST_IS_FLUGDLSINK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass), GST_TYPE_FLUGDLSINK))
typedef struct _GstFluGdlSink GstFluGdlSink;
typedef struct _GstFluGdlSinkClass GstFluGdlSinkClass;

typedef struct _GdlSurface GdlSurface;

typedef enum _GdlScaleMode {
  GDL_SCALE_MODE_SCALE_TO_FIT,
  GDL_SCALE_MODE_NONE,
  GDL_SCALE_MODE_ZOOM_TO_FILL,
  GDL_SCALE_MODE_ZOOM_TO_FIT
} GdlScaleMode;

struct _GstFluGdlSink
{
  /* Our element stuff */
  GstVideoSink videosink;

  /* < private > */

  /* current caps */
  GstCaps *caps_available;
  gint rowstride;
  gint par_n;
  gint par_d;

  /* gdl related */
  gdl_display_id_t display_id;

  gint tvmode;

  gdl_gamma_t gamma;

  gdl_plane_id_t video_plane;
  gdl_plane_id_t cc_plane;

  gint tv_width;
  gint tv_height;

  guint32 brightness;
  guint32 contrast;
  guint32 hue;
  guint32 aspect_ratio;

  /* rendering stuff */
  gdl_surface_info_t surfaces[NUM_SURFACES];
  gdl_surface_info_t clear_surface;
  gint cur_surface;
  gdl_palette_t palette;

  gboolean mute;
  gboolean xpsm;
  gboolean need_discont;
  
  GdlScaleMode scale_mode;
  gboolean setup;
  GValue rectangle;
};

struct _GstFluGdlSinkClass
{
  GstVideoSinkClass parent_class;
};

GType gst_flugdlsink_get_type (void);

G_END_DECLS
#endif /* __GST_FLU_GDL_SINK_H__ */
