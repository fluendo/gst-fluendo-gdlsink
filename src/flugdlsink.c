/*
    This file is provided under a dual BSD/LGPLv2.1 license.  When using
    or redistributing this file, you may do so under either license.

    LGPL LICENSE SUMMARY

    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    This library is free software; you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as
    published by the Free Software Foundation; either version 2.1 of the
    License.

    This library is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
    USA. The full GNU Lesser General Public License is included in this
    distribution in the file called LICENSE.LGPL.
    
    Contat Information for Fluendo:
        FLUENDO S.A.
        World Trade Center Ed Norte 4 pl.
        Moll de Barcelona
        08039 BARCELONA - SPAIN

    BSD LICENSE

    Copyright(c) 2009, 2010. Fluendo S.A. All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions
    are met:

      - Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
      - Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in
        the documentation and/or other materials provided with the
        distribution.
      - Neither the name of FLUENDO S.A. nor the names of its
        contributors may be used to endorse or promote products derived
        from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
    OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
    SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
    LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
    OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* Object header */
#include "flugdlsink.h"

/* Debugging category */
#include <gst/gstinfo.h>

/* Rectangle GValues handling */
#include "flugdlrectangle.h"

GST_DEBUG_CATEGORY_STATIC (gst_debug_flugdlsink);
#define GST_CAT_DEFAULT gst_debug_flugdlsink

#ifndef GST_CHECK_VERSION
#define GST_CHECK_VERSION(major,minor,micro)  \
    (GST_VERSION_MAJOR > (major) || \
     (GST_VERSION_MAJOR == (major) && GST_VERSION_MINOR > (minor)) || \
     (GST_VERSION_MAJOR == (major) && GST_VERSION_MINOR == (minor) && \
      GST_VERSION_MICRO >= (micro)))
#endif

#if GDL_HEADER_VERSION_MAJOR >= 75
#define GDL_PLANE_UPSCALE GDL_PLANE_SCALE
#endif

/* ElementFactory information */
static const GstElementDetails flugdlsink_details =
GST_ELEMENT_DETAILS ("Video sink",
    "Sink/Video",
    "A GDL based videosink",
    "Josep Torra <josep@fluendo.com>");

#define RGB_CAPS \
  "video/x-raw-rgb," \
  "  framerate = (fraction) [ 0/1, MAX ], " \
  "  width = (int) [ 1, MAX ], " \
  "  height = (int) [ 1, MAX ]," \
  "  bpp = (int) 32, " \
  "  endianness = (int) 4321, " \
  "  red_mask = (int) 65280, " \
  "  green_mask = (int) 16711680, " \
  "  blue_mask = (int) -16777216, " \
  "  alpha_mask = (int) 255, " \
  "  depth = (int) 32"

#define GRAY_CAPS \
  "video/x-raw-gray," \
  "  framerate = (fraction) [ 0/1, MAX ], " \
  "  width = (int) [ 1, MAX ], " \
  "  height = (int) [ 1, MAX ]," \
  "  bpp = (int) 8, " \
  "  depth = (int) 8"

#define ALL_CAPS \
  RGB_CAPS ";" GRAY_CAPS

static GstStaticPadTemplate flugdlsink_factory =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (ALL_CAPS));

enum
{
  PROP_UNKNOWN,
  PROP_TVMODE,
  PROP_GDL_PLANE,
  PROP_MUTE,
  PROP_XPSM,
  PROP_SCALE_MODE,
  PROP_RECTANGLE
};

GST_BOILERPLATE (GstFluGdlSink, gst_flugdlsink, GstVideoSink,
    GST_TYPE_VIDEO_SINK);

static void gst_flugdlsink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_flugdlsink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static gboolean gst_flugdlsink_start (GstBaseSink * bsink);
static gboolean gst_flugdlsink_stop (GstBaseSink * bsink);
static GstCaps *gst_flugdlsink_get_caps (GstBaseSink * bsink);
static gboolean gst_flugdlsink_set_caps (GstBaseSink * bsink,
    GstCaps * caps);
static GstFlowReturn gst_flugdlsink_render (GstBaseSink * bsink,
    GstBuffer * buf);
static gboolean gst_flugdlsink_event (GstBaseSink * bsink, GstEvent * event);

#define DEFAULT_TVMODE      -1
#define DEFAULT_PLANE       GDL_PLANE_ID_UPP_B
#define DEFAULT_DISPLAY_ID  GDL_DISPLAY_ID_0
#define DEFAULT_MUTE        FALSE
#define DEFAULT_XPSM        TRUE
#define DEFAULT_SCALE_MODE  GDL_SCALE_MODE_ZOOM_TO_FIT
#define DEFAULT_RECTANGLE   "0,0,0,0"

static const gdl_color_t color_black = { 0, 0, 0, 0 };

typedef struct _TVMode
{
  char *alias;
  gdl_uint32 width;
  gdl_uint32 height;
  gdl_refresh_t refresh;
  gdl_boolean_t interlaced;
} TVMode;

static const TVMode tvmodes[] = {
  {"480i", 720, 480, GDL_REFRESH_59_94, GDL_TRUE},
  {"480p", 720, 480, GDL_REFRESH_59_94, GDL_FALSE},
  {"576i", 720, 576, GDL_REFRESH_50, GDL_TRUE},
  {"576p", 720, 576, GDL_REFRESH_50, GDL_FALSE},
  {"720p", 1280, 720, GDL_REFRESH_59_94, GDL_FALSE},
  {"720p50", 1280, 720, GDL_REFRESH_50, GDL_FALSE},
  {"1080i", 1920, 1080, GDL_REFRESH_59_94, GDL_TRUE},
  {"1080i50", 1920, 1080, GDL_REFRESH_50, GDL_TRUE},
  {"1080p", 1920, 1080, GDL_REFRESH_59_94, GDL_FALSE},
  {"1080p50", 1920, 1080, GDL_REFRESH_50, GDL_FALSE},
  {NULL, 0, 0, (gdl_refresh_t) 0, GDL_FALSE}    // EOL
};

#define GST_TYPE_FLU_GDL_SINK_TVMODE \
  (gst_flu_gdl_sink_tvmode_get_type())
static GType
gst_flu_gdl_sink_tvmode_get_type (void)
{
  static GType gst_flu_gdl_sink_tvmode_type = 0;
  static const GEnumValue tvmode_types[] = {
    {-1, "None to be configured", "none"},
    {0, "480 interlaced at 60 Hz", "480i"},
    {1, "480 progressive at 60 Hz", "480p"},
    {2, "576 interlaced at 50 Hz", "576i"},
    {3, "576 progressive at 50 Hz", "576p"},
    {4, "720 progressive at 60 Hz", "720p"},
    {5, "720 progressive at 50 Hz", "720p50"},
    {6, "1080 interlaced at 60 Hz", "1080i"},
    {7, "1080 interlaced at 50 Hz", "1080i50"},
    {8, "1080 progressive at 60 Hz", "1080p"},
    {9, "1080 progressive at 50 Hz", "1080p50"},
    {10, NULL, NULL}
  };

  if (!gst_flu_gdl_sink_tvmode_type) {
    gst_flu_gdl_sink_tvmode_type =
        g_enum_register_static ("GstFluGdlSinkTVMode", tvmode_types);
  }
  return gst_flu_gdl_sink_tvmode_type;
}

#define GST_TYPE_FLU_GDL_SINK_PLANE \
  (gst_flu_gdl_sink_plane_get_type())
static GType
gst_flu_gdl_sink_plane_get_type (void)
{
  static GType gst_flu_gdl_sink_plane_type = 0;
  static const GEnumValue gdl_plane_types[] = {
    {GDL_PLANE_ID_IAP_A, "Indexed alpha plane A", "IAPA"},
    {GDL_PLANE_ID_IAP_B, "Indexed alpha plane B", "IAPB"},
    {GDL_PLANE_ID_UPP_A, "Universal Pixel Plane A", "UPPA"},
    {GDL_PLANE_ID_UPP_B, "Universal Pixel Plane B", "UPPB"},
    {GDL_PLANE_ID_UPP_C, "Universal Pixel Plane C", "UPPC"},
    {GDL_PLANE_ID_UPP_D, "Universal Pixel Plane D", "UPPD"},
    {10, NULL, NULL}
  };

  if (!gst_flu_gdl_sink_plane_type) {
    gst_flu_gdl_sink_plane_type =
        g_enum_register_static ("GstFluGdlSinkPlane", gdl_plane_types);
  }
  return gst_flu_gdl_sink_plane_type;
}

#define GST_TYPE_FLU_GDL_SINK_SCALE_MODE \
  (gst_flu_gdl_sink_scale_mode_get_type())
static GType
gst_flu_gdl_sink_scale_mode_get_type (void)
{
  static GType gst_flu_gdl_sink_scale_mode_type = 0;
  static const GEnumValue scale_mode_types[] = {
    {GDL_SCALE_MODE_NONE, /* NO_SCALING */
        "This will force the output to be same size as the input, ignores"
        " pixel aspect ratio.", "none"},
    {GDL_SCALE_MODE_SCALE_TO_FIT, /* SCALE_TO_FIT */
        "Basic independent x and y scaling, ignores pixel aspect ratio.",
        "scale2fit"},
    {GDL_SCALE_MODE_ZOOM_TO_FIT, /* ZOOM_TO_FIT */
        "Fit at least one side of the destination rectangle, letterbox/pillarbox"
        " the remainder.", "zoom2fit"},
    {GDL_SCALE_MODE_ZOOM_TO_FILL, /* ZOOM_TO_FILL */
        "Fill entire screen. Use both the source and destination pixel aspect"
        " ratios.", "zoom2fill"},
    {10, NULL, NULL}
  };

  if (!gst_flu_gdl_sink_scale_mode_type) {
    gst_flu_gdl_sink_scale_mode_type =
        g_enum_register_static ("GstFluGdlSinkScaleMode", scale_mode_types);
  }
  return gst_flu_gdl_sink_scale_mode_type;
}

static inline gdl_ret_t
set_gdl_port_attribute (GstFluGdlSink * sink, gdl_pd_id_t pd_id)
{
  gdl_display_id_t display_id = sink->display_id;
  gdl_uint32 port_brightness = sink->brightness;
  gdl_uint32 port_contrast = sink->contrast;
  gdl_uint32 port_hue = sink->hue;
  gdl_uint32 port_pic_aspect_ratio = sink->aspect_ratio;
  gdl_ret_t ret = GDL_SUCCESS;
  gdl_boolean_t enable_port = GDL_TRUE;

  ret = gdl_port_set_attr (pd_id, GDL_PD_ATTR_ID_POWER, &enable_port);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink,
        "gdl_port_set_attr (GDL_PD_ATTR_ID_POWER) failed %d", ret);
    return ret;
  }
  switch (pd_id) {
    case GDL_PD_ID_HDMI:
    {
      /* set hdmi port attribute */
      if (port_pic_aspect_ratio != 0) {
        ret =
            gdl_port_set_attr (pd_id, GDL_PD_ATTR_ID_PAR,
            &port_pic_aspect_ratio);
        if (ret != GDL_SUCCESS) {
          GST_ERROR_OBJECT (sink,
              "gdl_port_set_attr (PIXEL_FORMAT_OUTPUT) failed %d", ret);
          return ret;
        }
      }
      break;
    }
    case GDL_PD_ID_INTTVENC:
    case GDL_PD_ID_INTTVENC_COMPONENT:
    {
      /* set analog port attribute */
      if (port_brightness != 0) {
        ret =
            gdl_port_set_attr (pd_id, GDL_PD_ATTR_ID_BRIGHTNESS,
            &port_brightness);
        if (ret != GDL_SUCCESS) {
          GST_ERROR_OBJECT (sink,
              "gdl_port_set_attr (BRIGHTNESS) failed %d", ret);
          return ret;
        }
      }
      if (port_contrast != 0) {
        ret =
            gdl_port_set_attr (pd_id, GDL_PD_ATTR_ID_CONTRAST, &port_contrast);
        if (ret != GDL_SUCCESS) {

          GST_ERROR_OBJECT (sink,
              "gdl_port_set_attr (CONTRAST) failed %d", ret);
          return ret;
        }
      }
      if (pd_id == GDL_PD_ID_INTTVENC) {
        if (port_hue != 0) {
          ret = gdl_port_set_attr (pd_id, GDL_PD_ATTR_ID_HUE, &port_hue);
          if (ret != GDL_SUCCESS) {
            GST_ERROR_OBJECT (sink, "gdl_port_set_attr (HUE) failed %d", ret);
            return ret;
          }
        }
      }
      if (pd_id == GDL_PD_ID_INTTVENC_COMPONENT) {
        ret =
            gdl_port_set_attr (pd_id, GDL_PD_ATTR_ID_DISPLAY_PIPE, &display_id);
        if (ret != GDL_SUCCESS) {
          GST_ERROR_OBJECT (sink,
              "gdl_port_set_attr (DISPLAY_PIPE) failed %d", ret);
          return ret;
        }
      }
      break;
    }
    default:
      GST_ERROR_OBJECT (sink, "invalid pd_id");
      return GDL_ERR_FAILED;
  }
  return ret;
}

static inline gdl_ret_t
config_tvmode (GstFluGdlSink * sink)
{
  gdl_ret_t ret = GDL_SUCCESS;
  gdl_display_info_t di;

  GST_DEBUG_OBJECT (sink, "configure TV mode");
  memset (&di, 0, sizeof (di));

  if (sink->tvmode == -1) {
    /* Read current hardware state */
    ret = gdl_get_display_info (sink->display_id, &di);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "gdl_get_display_info failed %d", ret);
      goto beach;
    }
    GST_DEBUG_OBJECT (sink, "Current Disp_id: %d  %dx%d %d %d",
        di.id, di.tvmode.width, di.tvmode.height, di.tvmode.refresh,
        di.tvmode.interlaced);
  } else {
    di.tvmode.width = tvmodes[sink->tvmode].width;
    di.tvmode.height = tvmodes[sink->tvmode].height;
    di.tvmode.refresh = tvmodes[sink->tvmode].refresh;
    di.tvmode.interlaced = tvmodes[sink->tvmode].interlaced;
    GST_DEBUG_OBJECT (sink, "%s Disp_id: %d  %dx%d %d %d",
        tvmodes[sink->tvmode].alias, sink->display_id, di.tvmode.width,
        di.tvmode.height, di.tvmode.refresh, di.tvmode.interlaced);
  }

  /* HSD Defect 2753995. Improper background selection */
  if (di.tvmode.height > 576) {
    di.color_space = GDL_COLOR_SPACE_BT709;
  } else {
    di.color_space = GDL_COLOR_SPACE_BT601;
  }

  //di.bg_color = sink->bg_color;
  di.gamma = sink->gamma;
  di.flags = 0;
  di.id = sink->display_id;

  ret = gdl_set_display_info (&di);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_set_display_info failed %d", ret);
    goto beach;
  }
  /* Read back and print the new hardware state */
  ret = gdl_get_display_info (di.id, &di);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_get_display_info failed %d", ret);
    goto beach;
  }

  GST_DEBUG_OBJECT (sink, "Configured Disp_id: %d  %dx%d %d %d",
      di.id, di.tvmode.width, di.tvmode.height, di.tvmode.refresh,
      di.tvmode.interlaced);

  sink->tv_width = di.tvmode.width;
  sink->tv_height = di.tvmode.height;

beach:
  return ret;
}

static inline gdl_ret_t
config_upscale (GstFluGdlSink * sink, gdl_plane_id_t video_plane,
    gint w, gint h)
{
  gdl_ret_t ret;
  gdl_rectangle_t dest_rect, src_rect, final_rect;
  gdl_boolean_t upscale = GDL_TRUE;

  final_rect.origin.x = gst_value_get_rectangle_x (&sink->rectangle);
  final_rect.origin.y = gst_value_get_rectangle_y (&sink->rectangle);
  final_rect.width = gst_value_get_rectangle_width (&sink->rectangle);
  final_rect.height = gst_value_get_rectangle_height (&sink->rectangle);
  if (final_rect.origin.x == 0 && final_rect.origin.y == 0 &&
      final_rect.width == 0 && final_rect.height == 0) {
    final_rect.width = sink->tv_width;
    final_rect.height = sink->tv_height;
  }

  src_rect.origin.x = 0;
  src_rect.origin.y = 0;
  src_rect.width = w;
  src_rect.height = h;

  /* Begin GDL plane config */
  ret = gdl_plane_config_begin (video_plane);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_plane_config_begin failed %d", ret);
    goto exit;
  }

  /* Set GDL video plane dest rectangle attributes */
  ret = gdl_plane_get_attr (video_plane, GDL_PLANE_DST_RECT, &dest_rect);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink,
        "gdl_plane_get_attr (GDL_PLANE_DST_RECT) failed %d", ret);
    gdl_plane_config_end (GDL_TRUE);
    goto exit;
  }

  if (IS_IAP_PLANE (video_plane)) {
    gint line_replication, pixel_replication;

    dest_rect.origin.x = (sink->tv_width - w) / 2;
    dest_rect.origin.y = sink->tv_height - h;
    dest_rect.origin.x = dest_rect.origin.y = 0;
    dest_rect.width = w;
    dest_rect.height = h;

    if ((sink->tv_height / h) >= 2) {
      line_replication = pixel_replication = 2;
      dest_rect.origin.x = (sink->tv_width - (2 * w)) / 2;
      dest_rect.origin.y = sink->tv_height - (2 * h);
    } else {
      line_replication = pixel_replication = 1;
    }

    GST_DEBUG_OBJECT (sink, "line_replication %d pixel_replication %d",
        line_replication, pixel_replication);

    ret = gdl_plane_set_attr (GDL_PLANE_PIXEL_REPLICATION, &pixel_replication);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink,
          "gdl_plane_set_attr (GDL_PLANE_PIXEL_REPLICATION) failed %d", ret);
      gdl_plane_config_end (GDL_TRUE);
      goto exit;
    }

    ret = gdl_plane_set_attr (GDL_PLANE_LINE_REPLICATION, &line_replication);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink,
          "gdl_plane_set_attr (GDL_PLANE_LINE_REPLICATION) failed %d", ret);
      gdl_plane_config_end (GDL_TRUE);
      goto exit;
    }

  } else {
    /* This is a UPP plane, capable of upscaling. Check scaling mode and
     * configure src/dest rectangles appropriately */ 
    switch (sink->scale_mode) {
      case GDL_SCALE_MODE_NONE:
        GST_DEBUG_OBJECT (sink, "scale-mode is NONE");
         /* This will force the output to be same size as the input, ignores
          * pixel aspect ratio.*/
        dest_rect.origin.x = final_rect.origin.x;
        dest_rect.origin.y = final_rect.origin.y;
        dest_rect.width = src_rect.width;
        dest_rect.height = src_rect.height;
        break;
      case GDL_SCALE_MODE_SCALE_TO_FIT:
        GST_DEBUG_OBJECT (sink, "scale-mode is SCALE_TO_FIT");
        /* Basic independent x and y scaling, ignores pixel aspect ratio. */
        dest_rect.origin.x = final_rect.origin.x;
        dest_rect.origin.y = final_rect.origin.y;
        dest_rect.width = final_rect.width;
        dest_rect.height = final_rect.height;
        break;
      case GDL_SCALE_MODE_ZOOM_TO_FIT:
      {
        guint num, den;
        GST_DEBUG_OBJECT (sink, "scale-mode is ZOOM_TO_FIT");
        GstVideoRectangle gst_disp_rect;
        gst_disp_rect.x = final_rect.origin.x;
        gst_disp_rect.y = final_rect.origin.y;
        gst_disp_rect.w = final_rect.width;
        gst_disp_rect.h = final_rect.height;
        GstVideoRectangle gst_src_rect = {
            src_rect.origin.x, src_rect.origin.y,
            src_rect.width, src_rect.height
        };
        GstVideoRectangle gst_dest_rect;
        /* Fit at least one side of the destination rectangle, letterbox or
         * pillarbox the remainder */
        if (!gst_video_calculate_display_ratio (&num, &den, w, h, sink->par_n,
            sink->par_d, 1, 1)) {
          GST_ERROR_OBJECT (sink,
              "Failed to calculate the display aspect ratio");
          dest_rect = src_rect;
        } else {
          GST_DEBUG_OBJECT (sink,
              "video size: %dx%d video PAR: %d/%d -> display PAR: %d/%d",
              w, h, sink->par_n, sink->par_d, num, den);
          gst_src_rect.w = (gint)((gdouble)gst_src_rect.h * num / den + 0.5);
        }
        gst_video_sink_center_rect (gst_src_rect, gst_disp_rect, &gst_dest_rect,
            TRUE);
        dest_rect.origin.x = gst_dest_rect.x + final_rect.origin.x;
        dest_rect.origin.y = gst_dest_rect.y + final_rect.origin.y;
        dest_rect.width = gst_dest_rect.w;
        dest_rect.height = gst_dest_rect.h;
        break;
      }
      case GDL_SCALE_MODE_ZOOM_TO_FILL:
      {
        guint num, den;
        gdouble scale_w, scale_h;
        GST_DEBUG_OBJECT (sink, "scale-mode is ZOOM_TO_FILL");
        /* Fill entire screen. Use both the source and destination pixel aspect
         * ratios. */
        if (!gst_video_calculate_display_ratio (&num, &den, w, h, sink->par_n,
            sink->par_d, 1, 1)) {
          GST_ERROR_OBJECT (sink,
              "Failed to calculate the display aspect ratio");
          dest_rect = src_rect;
        } else {
          GST_DEBUG_OBJECT (sink,
              "video size: %dx%d video PAR: %d/%d -> display PAR: %d/%d",
              w, h, sink->par_n, sink->par_d, num, den);
          src_rect.width = (gint)((gdouble)src_rect.height * num / den + 0.5);
          GST_DEBUG_OBJECT (sink,
              "corrected size: %dx%d",src_rect.width, src_rect.height);
          /* Calculate size of source rectagle */
          scale_w = (gdouble)final_rect.width / src_rect.width;
          scale_h = (gdouble)final_rect.height / src_rect.height;
          GST_DEBUG_OBJECT (sink,
              "scale factors: w=%g h=%g",scale_w, scale_h);
          if (scale_w > scale_h) {
            src_rect.width = w;
            src_rect.origin.x = 0;
            src_rect.height = final_rect.height / scale_w;
            src_rect.origin.y = (h - src_rect.height) / 2;
          } else {
            src_rect.height = h;
            src_rect.origin.y = 0;
            src_rect.width = final_rect.width / scale_h;
            src_rect.origin.x = (w - src_rect.width) / 2;
          }
          /* Make dest rectangle fill entire screen */
          dest_rect.origin.x = final_rect.origin.x;
          dest_rect.origin.y = final_rect.origin.y;
          dest_rect.width = final_rect.width;
          dest_rect.height = final_rect.height;
        }
        break;
      }
    }
  }

  GST_DEBUG_OBJECT (sink, "dest rectangle is (%d, %d, %d ,%d)",
      dest_rect.origin.x, dest_rect.origin.y,
      dest_rect.width, dest_rect.height);

  ret = gdl_plane_set_attr (GDL_PLANE_DST_RECT, &dest_rect);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink,
        "gdl_plane_set_attr (GDL_PLANE_DST_RECT) failed %d", ret);
    gdl_plane_config_end (GDL_TRUE);
    goto exit;
  }

  GST_DEBUG_OBJECT (sink, "src rectangle is (%d, %d, %d ,%d)",
      src_rect.origin.x, src_rect.origin.y,
      src_rect.width, src_rect.height);

  /* Set GDL video plane src rectangle attributes */
  ret = gdl_plane_set_attr (GDL_PLANE_SRC_RECT, &src_rect);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink,
        "gdl_plane_set_attr (GDL_PLANE_SRC_RECT) failed %d", ret);
    gdl_plane_config_end (GDL_TRUE);
    goto exit;
  }
  
  upscale =
      dest_rect.width > src_rect.width ||
      dest_rect.height > src_rect.height;
  GST_DEBUG_OBJECT (sink, "GDL upscale is %d", upscale);

  if (IS_IAP_PLANE (video_plane)) {
    GST_DEBUG_OBJECT (sink, "IAP plane isn't capable to do upscaling");
  } else {
    /* Set GDL video plane upscale attributes */
    ret = gdl_plane_set_attr (GDL_PLANE_UPSCALE, &upscale);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink,
          "gdl_plane_get_attr (GDL_PLANE_UPSCALE) failed %d", ret);
      gdl_plane_config_end (GDL_TRUE);
      goto exit;
    }
  }

  /* End GDL plane config */
  ret = gdl_plane_config_end (GDL_FALSE);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_plane_config_end failed %d", ret);
    gdl_plane_config_end (GDL_TRUE);
    goto exit;
  }

exit:
  return ret;
}

static inline gdl_ret_t
unconfig_upscale (GstFluGdlSink * sink, gdl_plane_id_t video_plane)
{
  gdl_ret_t ret;
  gdl_boolean_t upscale = GDL_FALSE;

  if (IS_IAP_PLANE (video_plane)) {
    GST_DEBUG_OBJECT (sink, "IAP plane isn't capable to do upscaling");
    ret = GDL_SUCCESS;
    goto exit;
  }
  /* Begin GDL plane config */
  ret = gdl_plane_config_begin (video_plane);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_plane_config_begin failed %d", ret);
    goto exit;
  }

  /* Set GDL video plane upscale attributes */
  ret = gdl_plane_set_attr (GDL_PLANE_UPSCALE, &upscale);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink,
        "gdl_plane_get_attr (GDL_PLANE_UPSCALE) failed %d", ret);
    gdl_plane_config_end (GDL_TRUE);
    goto exit;
  }

  /* End GDL plane config */
  ret = gdl_plane_config_end (GDL_FALSE);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_plane_config_end failed %d", ret);
    gdl_plane_config_end (GDL_TRUE);
    goto exit;
  }

exit:
  return ret;
}

static inline gdl_ret_t
config_plane (GstFluGdlSink * sink, gdl_plane_id_t video_plane)
{
  gdl_ret_t ret;
  gdl_display_info_t di;
  gdl_pixel_format_t pix_fmt = GDL_PF_ARGB_32;
  gdl_color_space_t color_space = GDL_COLOR_SPACE_RGB;
  guint32 tv_width;
  guint32 tv_height;

  ret = GDL_SUCCESS;

  GST_DEBUG_OBJECT (sink, "doing config_plane");
  memset (&di, 0, sizeof (di));

  tv_width = sink->tv_width;
  tv_height = sink->tv_height;

  /* Reset GDL video plane */
  ret = gdl_plane_reset (video_plane);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_plane_reset failed %d", ret);
    goto exit;
  }
  /* Begin GDL plane config */
  ret = gdl_plane_config_begin (video_plane);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_plane_config_begin failed %d", ret);
    goto exit;
  }

  if (IS_IAP_PLANE (video_plane)) {
    gdl_uint32 alpha_global = 255;
    gdl_uint32 alpha_out = GDL_PLANE_ID_UNDEFINED;
    GST_DEBUG_OBJECT (sink, "an IAP plane is going to be configured");

    /* Set GDL alpha global related attributes */
    ret = gdl_plane_set_attr (GDL_PLANE_ALPHA_GLOBAL, &alpha_global);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink,
          "gdl_plane_set_attr (GDL_PLANE_ALPHA_GLOBAL) failed %d", ret);
      gdl_plane_config_end (GDL_TRUE);
      goto exit;
    }
    /* Set GDL alpha global related attributes */
    ret = gdl_plane_set_attr (GDL_PLANE_ALPHA_OUT, &alpha_out);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink,
          "gdl_plane_set_attr (GDL_PLANE_ALPHA_OUT) failed %d", ret);
      gdl_plane_config_end (GDL_TRUE);
      goto exit;
    }
    pix_fmt = GDL_PF_ARGB_8;
  }

  /* Set GDL colorspace related attributes */
  ret = gdl_plane_set_attr (GDL_PLANE_SRC_COLOR_SPACE, &color_space);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink,
        "gdl_plane_set_attr (GDL_PLANE_SRC_COLOR_SPACE) failed %d", ret);
    gdl_plane_config_end (GDL_TRUE);
    goto exit;
  }

  ret = gdl_plane_set_attr (GDL_PLANE_PIXEL_FORMAT, &pix_fmt);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink,
        "gdl_plane_set_attr(GDL_PLANE_PIXEL_FORMAT) failed %d", ret);
    gdl_plane_config_end (GDL_TRUE);
    goto exit;
  }

  /* End GDL plane config */
  ret = gdl_plane_config_end (GDL_FALSE);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_plane_config_end failed %d", ret);
    gdl_plane_config_end (GDL_TRUE);
    goto exit;
  }

exit:
  return ret;
}

static gboolean
reconfig_plane (GstFluGdlSink * sink, gdl_plane_id_t video_plane)
{
  gdl_ret_t st;
  if (sink->setup) {
    st = config_plane (sink, video_plane);
    if (st != GDL_SUCCESS) {
      GST_WARNING_OBJECT (sink, "configure plane failed(%d)", st);
      goto error;
    }
    if (GST_VIDEO_SINK_WIDTH (sink) && GST_VIDEO_SINK_HEIGHT (sink)) {
      unconfig_upscale (sink, sink->video_plane);
      config_upscale (sink, video_plane, GST_VIDEO_SINK_WIDTH (sink),
          GST_VIDEO_SINK_HEIGHT (sink));
    }
  }
  sink->video_plane = video_plane;
  GST_INFO_OBJECT (sink, "gdl plane succefully reconfigured to %d",
      video_plane);
  return TRUE;
error:
  return FALSE;
}

static gboolean
setup_gdl (GstFluGdlSink * sink)
{
  gboolean ret = FALSE;
  gdl_ret_t st;

  if (gdl_init (0) != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_init failed");
    goto beach;
  }

  /* output to all port */
  if (sink->tvmode != -1) {  
    /* try to activate output to all ports */
    st = set_gdl_port_attribute (sink, GDL_PD_ID_INTTVENC_COMPONENT);
    if (st != GDL_SUCCESS) {
      GST_WARNING_OBJECT (sink, "set_gdl_port_attribute failed! ret: %d", ret);
    }

    st = set_gdl_port_attribute (sink, GDL_PD_ID_HDMI);
    if (st != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "set_gdl_port_attribute failed! ret: %d", ret);
      goto error;
    }
  }

  st = config_tvmode (sink);
  if (st != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "configure tvmode failed", st);
    goto error;
  }

  st = config_plane (sink, sink->video_plane);
  if (st != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "configure plane failed");
    goto error;
  }

  ret = TRUE;
beach:
  return ret;

error:
  gdl_close ();
  return FALSE;
}

static inline void
reset (GstFluGdlSink * sink)
{
  gint i;
  for (i = 0; i < NUM_SURFACES; i++) {
    sink->surfaces[i].id = GDL_SURFACE_INVALID;
  }
  sink->clear_surface.id = GDL_SURFACE_INVALID;
  sink->cur_surface = 0;
}

static inline gdl_ret_t
alloc_surfaces (GstFluGdlSink * sink, gint width, gint height)
{
  gint i;
  gdl_ret_t ret = GDL_SUCCESS;
  gdl_pixel_format_t pix_format = GDL_PF_ARGB_32;

  if (IS_IAP_PLANE (sink->video_plane)) {
    pix_format = GDL_PF_ARGB_8;
  }

  for (i = 0; i < NUM_SURFACES; i++) {
    ret = gdl_alloc_surface (pix_format, width, height, 0, &sink->surfaces[i]);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "gdl_alloc_surface failed %d", ret);
      goto beach;
    }
    if (IS_IAP_PLANE (sink->video_plane)) {
      ret = gdl_set_palette (sink->surfaces[i].id, &sink->palette);
      if (ret != GDL_SUCCESS) {
        GST_ERROR_OBJECT (sink, "gdl_set_palette failed %d", ret);
        goto beach;
      }
    }
    ret =
        gdl_clear_surface (sink->surfaces[i].id, (gdl_color_t *) & color_black);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "gdl_clear_surface failed %d", ret);
      goto beach;
    }

  }

  ret = gdl_alloc_surface (pix_format, width, height, 0, &sink->clear_surface);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_alloc_surface failed %d", ret);
    goto beach;
  }

  if (IS_IAP_PLANE (sink->video_plane)) {
    ret = gdl_set_palette (sink->clear_surface.id, &sink->palette);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "gdl_set_palette failed %d", ret);
      goto beach;
    }
  }

  ret =
      gdl_clear_surface (sink->clear_surface.id, (gdl_color_t *) & color_black);
  if (ret != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_clear_surface failed %d", ret);
    goto beach;
  }

beach:
  return ret;
}

static inline gdl_ret_t
dealloc_surfaces (GstFluGdlSink * sink)
{
  gint i;
  gdl_ret_t ret = GDL_SUCCESS;
  for (i = 0; i < NUM_SURFACES; i++) {
    if (sink->surfaces[i].id != GDL_SURFACE_INVALID) {
      ret = gdl_free_surface (sink->surfaces[i].id);
      if (ret != GDL_SUCCESS) {
        GST_ERROR_OBJECT (sink, "gdl_alloc_surface failed %d", ret);
        goto beach;
      }
      sink->surfaces[i].id = GDL_SURFACE_INVALID;
    }
  }

  if (sink->clear_surface.id != GDL_SURFACE_INVALID) {
    ret = gdl_free_surface (sink->clear_surface.id);
    if (ret != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "gdl_alloc_surface failed %d", ret);
      goto beach;
    }
    sink->clear_surface.id = GDL_SURFACE_INVALID;
  }

beach:
  return ret;
}

static inline GstCaps *
get_available_caps (GstFluGdlSink * sink)
{
  GstCaps *caps = NULL;

  if (IS_IAP_PLANE (sink->video_plane)) {
    caps = gst_caps_from_string (GRAY_CAPS);
    GST_INFO_OBJECT (sink, "Added %s", GRAY_CAPS);
  } else {
    caps = gst_caps_from_string (RGB_CAPS);
    GST_INFO_OBJECT (sink, "Added %s", RGB_CAPS);
  }
  return caps;
}

static gboolean
gst_flugdlsink_start (GstBaseSink * bsink)
{
  GstFluGdlSink *sink = GST_FLUGDLSINK (bsink);

  GST_LOG_OBJECT (sink, "start");

  if (!(sink->setup = setup_gdl (sink))) {
    GST_ERROR_OBJECT (sink, "setup_gdl failed");
    return FALSE;
  }

  sink->caps_available = get_available_caps (sink);

  return TRUE;
}

static gboolean
gst_flugdlsink_stop (GstBaseSink * bsink)
{
  GstFluGdlSink *sink = GST_FLUGDLSINK (bsink);

  GST_VIDEO_SINK_WIDTH (sink) = 0;
  GST_VIDEO_SINK_HEIGHT (sink) = 0;

  if (sink->caps_available) {
    gst_caps_unref (sink->caps_available);
    sink->caps_available = NULL;
  }

  sink->par_n = 0;
  sink->par_d = 0;
  dealloc_surfaces (sink);
  reset (sink);
  unconfig_upscale (sink, sink->video_plane);
  gdl_close ();
  GST_LOG_OBJECT (sink, "stop");
  sink->setup = FALSE;
  return TRUE;
}

static inline void
load_grayscale_palette (GstFluGdlSink * sink)
{
  gint i;

  GST_DEBUG_OBJECT (sink, "loading grayscale palette");
  sink->palette.data[0].a = 0x00;
  sink->palette.data[0].r_y = 0x00;
  sink->palette.data[0].g_u = 0x00;
  sink->palette.data[0].b_v = 0x00;

  for (i = 1; i < 256; i++) {
    sink->palette.data[i].a = 0xff;
    sink->palette.data[i].r_y = i;
    sink->palette.data[i].g_u = i;
    sink->palette.data[i].b_v = i;
  }
}

static GstCaps *
gst_flugdlsink_get_caps (GstBaseSink * bsink)
{
  GstFluGdlSink *sink = GST_FLUGDLSINK (bsink);
  GstCaps *caps;

  if (G_LIKELY (sink->caps_available))
    caps = gst_caps_ref (sink->caps_available);
  else
    caps = gst_caps_copy (gst_pad_get_pad_template_caps (bsink->sinkpad));

  return caps;
}

static gboolean
gst_flugdlsink_set_caps (GstBaseSink * bsink, GstCaps * caps)
{
  GstFluGdlSink *sink = GST_FLUGDLSINK (bsink);
  GstStructure *structure;
  const gchar *name = NULL;
  gint w, h, par_n, par_d;

  GST_LOG_OBJECT (sink, "caps: %" GST_PTR_FORMAT, caps);

  structure = gst_caps_get_structure (caps, 0);
  name = gst_structure_get_name (structure);

  if (!gst_structure_get_int (structure, "width", &w) ||
      !gst_structure_get_int (structure, "height", &h)) {
    GST_ERROR_OBJECT (sink, "missing parameters from the caps (required "
        "width, height)");
    return FALSE;
  }

  if (IS_IAP_PLANE (sink->video_plane)) {
    if (!strcmp (name, "video/x-raw-gray")) {
      load_grayscale_palette (sink);
    } else {
      GST_WARNING_OBJECT (sink, "IAP plane only supports grayscale");
      return FALSE;
    }
    sink->rowstride = w;
  } else {
    sink->rowstride = w * 4;
  }

  if (!gst_video_parse_caps_pixel_aspect_ratio (caps, &par_n, &par_d)) {
    GST_LOG_OBJECT (sink, "no pixel aspect ratio, assuming 1/1");
    par_n = par_d = 1;
  }

  sink->par_n = par_n;
  sink->par_d = par_d;

  if (GST_VIDEO_SINK_WIDTH (sink) != w && GST_VIDEO_SINK_HEIGHT (sink) != h) {
    if (dealloc_surfaces (sink) != GDL_SUCCESS)
      return FALSE;
    if (alloc_surfaces (sink, w, h) != GDL_SUCCESS)
      return FALSE;
    if (config_upscale (sink, sink->video_plane, w, h) != GDL_SUCCESS)
      return FALSE;
  }

  GST_VIDEO_SINK_WIDTH (sink) = w;
  GST_VIDEO_SINK_HEIGHT (sink) = h;

  GST_INFO_OBJECT (sink, "width x height     : %d x %d", w, h);
  GST_INFO_OBJECT (sink, "pixel-aspect-ratio : %d/%d", par_n, par_d);

  return TRUE;
}

/* with STREAM_LOCK, PREROLL_LOCK
 * adjust a timestamp with the latency and timestamp offset */
static inline GstClockTime
adjust_time (GstBaseSink * bsink, GstClockTime time)
{
  GstClockTimeDiff ts_offset;

  /* don't do anything funny with invalid timestamps */
  if (G_UNLIKELY (!GST_CLOCK_TIME_IS_VALID (time)))
    return time;

  time += gst_base_sink_get_latency (bsink);

  /* apply offset, be carefull for underflows */
  ts_offset = gst_base_sink_get_ts_offset (bsink);
  if (ts_offset < 0) {
    ts_offset = -ts_offset;
    if (ts_offset < time)
      time -= ts_offset;
    else
      time = 0;
  } else
    time += ts_offset;

  return time;
}

/**
 * gst_base_sink_wait_clock:
 * @sink: the sink
 * @time: the running_time to be reached
 * @jitter: the jitter to be filled with time diff (can be NULL)
 *
 * This function will block until @time is reached. It is usually called by
 * subclasses that use their own internal synchronisation.
 *
 * If @time is not valid, no sycnhronisation is done and #GST_CLOCK_BADTIME is
 * returned. Likewise, if synchronisation is disabled in the element or there
 * is no clock, no synchronisation is done and #GST_CLOCK_BADTIME is returned.
 *
 * This function should only be called with the PREROLL_LOCK held, like when
 * receiving an EOS event in the ::event vmethod or when receiving a buffer in
 * the ::render vmethod.
 *
 * The @time argument should be the running_time of when this method should
 * return and is not adjusted with any latency or offset configured in the
 * sink.
 *
 * Since 0.10.20
 *
 * Returns: #GstClockReturn
 */
GstClockReturn
wait_clock (GstBaseSink * sink, GstClockTime time, GstClockTimeDiff * jitter)
{
  GstClockID id;
  GstClockReturn ret;
  GstClock *clock;

  if (G_UNLIKELY (!GST_CLOCK_TIME_IS_VALID (time)))
    goto invalid_time;

  GST_OBJECT_LOCK (sink);
  if (G_UNLIKELY (!sink->sync))
    goto no_sync;

  if (G_UNLIKELY ((clock = GST_ELEMENT_CLOCK (sink)) == NULL))
    goto no_clock;

  /* add base_time to running_time to get the time against the clock */
  time += GST_ELEMENT_CAST (sink)->base_time;

  id = gst_clock_new_single_shot_id (clock, time);
  GST_OBJECT_UNLOCK (sink);

  /* A blocking wait is performed on the clock. We save the ClockID
   * so we can unlock the entry at any time. While we are blocking, we
   * release the PREROLL_LOCK so that other threads can interrupt the
   * entry. */
  sink->clock_id = id;
  /* release the preroll lock while waiting */
  GST_PAD_PREROLL_UNLOCK (sink->sinkpad);

  ret = gst_clock_id_wait (id, jitter);

  GST_PAD_PREROLL_LOCK (sink->sinkpad);
  gst_clock_id_unref (id);
  sink->clock_id = NULL;

  return ret;

  /* no syncing needed */
invalid_time:
  {
    GST_DEBUG_OBJECT (sink, "time not valid, no sync needed");
    return GST_CLOCK_BADTIME;
  }
no_sync:
  {
    GST_DEBUG_OBJECT (sink, "sync disabled");
    GST_OBJECT_UNLOCK (sink);
    return GST_CLOCK_BADTIME;
  }
no_clock:
  {
    GST_DEBUG_OBJECT (sink, "no clock, can't sync");
    GST_OBJECT_UNLOCK (sink);
    return GST_CLOCK_BADTIME;
  }
}

static inline GstClockTime
calculate_stime (GstFluGdlSink * sink, GstBuffer * buf)
{
  GstClockTime render_delay = 0, stop, rstop, stime;
  GstBaseSink *bsink = GST_BASE_SINK (sink);

  stop = GST_BUFFER_TIMESTAMP (buf);
  if (GST_CLOCK_TIME_IS_VALID (GST_BUFFER_DURATION (buf))) {
    stop += GST_BUFFER_DURATION (buf);
  } else {
    GST_DEBUG_OBJECT (sink, "no duration");
    return GST_CLOCK_TIME_NONE;
  }

#if GST_CHECK_VERSION(0,10,21)
  render_delay = gst_base_sink_get_render_delay (bsink);
#endif

  rstop =
      gst_segment_to_running_time (&bsink->segment, GST_FORMAT_TIME, stop);
  stime = adjust_time (bsink, rstop);

  /* adjust for render-delay, avoid underflows */
  if (stime != -1) {
    if (stime > render_delay)
      stime -= render_delay;
    else
      stime = 0;
  }

  /* rendering of this buffer will end at stime */
  GST_DEBUG_OBJECT (sink, "end rendering time %" GST_TIME_FORMAT
      ", adjusted %" GST_TIME_FORMAT ", ts %" GST_TIME_FORMAT
      " --> %" GST_TIME_FORMAT,
      GST_TIME_ARGS (rstop), GST_TIME_ARGS (stime),
      GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buf)), GST_TIME_ARGS (stop));

  return stime;
}

static inline gboolean
is_too_late (GstFluGdlSink * sink, GstClockTime time)
{
  gboolean ret = FALSE;
  GstClock *clock;

  if (!GST_CLOCK_TIME_IS_VALID (time)) {
    goto beach;
  }

  GST_OBJECT_LOCK (sink);
  if (G_UNLIKELY ((clock = GST_ELEMENT_CLOCK (sink)) == NULL))
    goto no_clock;

  /* add base_time to running_time to get the time against the clock */
  time += GST_ELEMENT_CAST (sink)->base_time;

  ret = (time < gst_clock_get_time (clock));
  GST_OBJECT_UNLOCK (sink);

beach:
  return ret;

no_clock:
  {
    GST_DEBUG_OBJECT (sink, "no clock, can't sync");
    GST_OBJECT_UNLOCK (sink);
    return FALSE;
  }
}

static inline gboolean
wait_duration (GstFluGdlSink * sink, GstClockTime stime)
{
  GstClockReturn status = GST_CLOCK_OK;
  GstBaseSink *bsink = GST_BASE_SINK (sink);
  gboolean mute;

  GST_OBJECT_LOCK (sink);
  mute = sink->mute;
  GST_OBJECT_UNLOCK (sink);

  /* preroll done, we can sync since we are in PLAYING now. */
  GST_DEBUG_OBJECT (sink, "waiting for clock to reach %" GST_TIME_FORMAT,
      GST_TIME_ARGS (stime));

  /* This function will return immediatly if start == -1, no clock
   * or sync is disabled with GST_CLOCK_BADTIME. */
  status = wait_clock (bsink, stime, NULL);

  GST_DEBUG_OBJECT (sink, "clock returned %d", status);

  /* invalid time, no clock or sync disabled, just render */
  if (status == GST_CLOCK_BADTIME)
    return FALSE;

  /* waiting could have been interrupted and we can be flushing now */
  if (G_UNLIKELY (bsink->flushing))
    return TRUE;

  /* check for unlocked by a state change, we are not flushing so
   * we can try to preroll on the current buffer. */
  if (G_UNLIKELY (status == GST_CLOCK_UNSCHEDULED)) {
    gboolean current_mute;
    GST_OBJECT_LOCK (sink);
    current_mute = sink->mute;
    GST_OBJECT_UNLOCK (sink);
    if (mute != current_mute)
      return TRUE;
    else
      return FALSE;
  }
  return TRUE;
}

static inline gdl_ret_t
buffer_2_iap (GstFluGdlSink * sink, guint8 * data, gdl_surface_id_t surface_id)
{
  gdl_ret_t st;
  guint8 *surf_ptr;
  guint surf_pitch;
  guint row;
  guint width = GST_VIDEO_SINK_WIDTH (sink);
  guint height = GST_VIDEO_SINK_HEIGHT (sink);
  gint rowstride = sink->rowstride;

  st = gdl_map_surface (surface_id, (void *) &surf_ptr, &surf_pitch);
  if (st != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_map_surface failed %d", st);
  }

  if (surf_pitch == rowstride) {
    GST_DEBUG_OBJECT (sink, "copying buffer data, fast path");
    memcpy (surf_ptr, data, rowstride * height);
  } else {
    GST_DEBUG_OBJECT (sink, "copying buffer data, slow path %d %d",
        surf_pitch, rowstride);
    for (row = 0; row < height; row++) {
      memcpy (surf_ptr, data, (surf_pitch ? surf_pitch : width));
      surf_ptr += (surf_pitch ? surf_pitch : width);
      data += rowstride;
    }
  }
  st = gdl_unmap_surface (surface_id);
  if (st != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_unmap_surface failed %d", st);
  }
  return st;
}

static GstFlowReturn
gst_flugdlsink_render (GstBaseSink * bsink, GstBuffer * buf)
{
  GstFluGdlSink *sink;
  gdl_surface_id_t id;
  gdl_plane_id_t video_plane;
  gdl_ret_t st;
  GstClockTime stime;
  gboolean mute, need_discont;

  sink = GST_FLUGDLSINK (bsink);

  GST_OBJECT_LOCK (sink);
  mute = sink->mute;
  need_discont = sink->need_discont;
  video_plane = sink->video_plane;  
  GST_OBJECT_UNLOCK (sink);

  if (G_UNLIKELY (need_discont)) {
    GST_DEBUG_OBJECT (sink, "we need a buffer with discont flag");
    if (G_UNLIKELY (!GST_BUFFER_FLAG_IS_SET (buf, GST_BUFFER_FLAG_DISCONT))) {
      GST_DEBUG_OBJECT (sink, "discont flag is not set, dropping buffer %p",
          buf);
      goto beach;
    }
    GST_DEBUG_OBJECT (sink, "discont flag found on buffer %p", buf);
    GST_OBJECT_LOCK (sink);
    sink->need_discont = FALSE;
    GST_OBJECT_UNLOCK (sink);
  }

  /* Skip rendering of filler buffers with the GAP flag set */
  if (G_UNLIKELY (GST_BUFFER_FLAG_IS_SET (buf, GST_BUFFER_FLAG_GAP))) {
    GST_LOG_OBJECT (sink, "render clear surface");
    st = gdl_flip (video_plane, sink->clear_surface.id, GDL_FLIP_SYNC);
    if (st != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "gdl_flip failed %d", st);
    }
    GST_DEBUG_OBJECT (sink, "dropping filler buffer");
    goto beach;
  }

  /* If the element is muted just drop the buffer */
  if (G_UNLIKELY (mute)) {
    GST_DEBUG_OBJECT (sink, "sink is muted, dropping buffer");
    goto beach;
  }

  /* stime is the time instant that the buffer rendering is ended expresed
   * in running time */
  stime = calculate_stime (sink, buf);
  /* If we receive older buffers just drop them */
  if (G_UNLIKELY (is_too_late (sink, stime))) {
    GST_DEBUG_OBJECT (sink, "buffer is too late, dropping it");
    goto beach;
  }

  /* Manage our surface pool */
  id = sink->surfaces[sink->cur_surface].id;
  sink->cur_surface = (sink->cur_surface + 1) % NUM_SURFACES;

  /* Put the buffer data in the surface */
  if (IS_IAP_PLANE (video_plane)) {
    st = buffer_2_iap (sink, GST_BUFFER_DATA (buf), id);
    if (st != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "buffer_2_iap %d", st);
    }
  } else {
#if 0
    /* Add a thick border to the GDL buffer. Userful for debug */
    int i, j;
    int w = GST_VIDEO_SINK_WIDTH (sink);
    int h = GST_VIDEO_SINK_HEIGHT (sink);
    guint32 *data = (guint32*) GST_BUFFER_DATA (buf);
    for (j=0; j<5; j++) {
      for (i=0; i<w; i++) {
        data[i+j*w] = 0xffffffff;
        data[(h-1)*w + i - j*w] = 0xffffffff;
      }
      for (i=0; i<h; i++) {
        data[i*w+j] = 0xffffffff;
        data[(w-1)+ i*w -j] = 0xffffffff;
      }
    }
#endif
    st = gdl_put (id, NULL, sink->rowstride, GST_BUFFER_DATA (buf), 0);
    if (st != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "gdl_put failed %d", st);
    }
  }
  /* Display the surface in the plane */
  GST_LOG_OBJECT (sink, "render buffer %p with ts %" GST_TIME_FORMAT
      " and duration %" GST_TIME_FORMAT " in surface id %d",
      buf, GST_TIME_ARGS (GST_BUFFER_TIMESTAMP(buf)),
      GST_TIME_ARGS (GST_BUFFER_DURATION(buf)), id);
  st = gdl_flip (video_plane, id, GDL_FLIP_SYNC);
  if (st != GDL_SUCCESS) {
    GST_ERROR_OBJECT (sink, "gdl_flip failed %d", st);
  }

  if (G_LIKELY (GST_CLOCK_TIME_IS_VALID (stime))) {
    if (G_LIKELY (wait_duration (sink, stime))) {
      GST_LOG_OBJECT (sink, "render clear surface");
      st = gdl_flip (video_plane, sink->clear_surface.id, GDL_FLIP_SYNC);
      if (st != GDL_SUCCESS) {
        GST_ERROR_OBJECT (sink, "gdl_flip failed %d", st);
      }
    }
  }

beach:
  return GST_FLOW_OK;
}

static gboolean
gst_flugdlsink_event (GstBaseSink * bsink, GstEvent * event)
{
  GstFluGdlSink *sink = GST_FLUGDLSINK (bsink);

  GST_LOG_OBJECT (sink, "handling %s event", GST_EVENT_TYPE_NAME (event));
  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_CUSTOM_DOWNSTREAM_OOB:
    {
      const GstStructure *s;

      s = gst_event_get_structure (event);

      if (gst_structure_has_name (s, "GstFluSubSelectorChange")) {
        GST_OBJECT_LOCK (sink);
        sink->need_discont = TRUE;
        GST_OBJECT_UNLOCK (sink);

        GST_PAD_PREROLL_LOCK (bsink->sinkpad);
        if (bsink->clock_id) {
          GST_DEBUG_OBJECT (sink, "unschedule duration active wait");
          gst_clock_id_unschedule (bsink->clock_id);
        }
        GST_PAD_PREROLL_UNLOCK (bsink->sinkpad);
      }
      return TRUE;
    }
    case GST_EVENT_FLUSH_STOP:
    {
      GST_OBJECT_LOCK (sink);
      sink->need_discont = TRUE;
      GST_OBJECT_UNLOCK (sink);
      break;
    }    
    case GST_EVENT_NEWSEGMENT:
    {
      GST_OBJECT_LOCK (sink);
      sink->need_discont = FALSE;
      GST_OBJECT_UNLOCK (sink);
      break;
    }    
    default:
      break;
  }

  if (GST_BASE_SINK_CLASS (parent_class)->event) {
    return GST_BASE_SINK_CLASS (parent_class)->event (bsink, event);
  } else {
    return TRUE;
  }
}

static GstStateChangeReturn
gst_flugdlsink_change_state (GstElement * element, GstStateChange transition)
{
  GstStateChangeReturn ret = GST_STATE_CHANGE_SUCCESS;
  GstFluGdlSink *sink = GST_FLUGDLSINK (element);
  GstBaseSink *bsink = GST_BASE_SINK (sink);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      GST_OBJECT_LOCK (sink);
      /* This is a hack to match the XPSM timming hack that changes the
       * base time after a seek */
      if (sink->xpsm) {
        GST_DEBUG_OBJECT (sink, "current base time: %" GST_TIME_FORMAT,
            GST_TIME_ARGS(element->base_time));
        element->base_time += bsink->segment.time;
        GST_DEBUG_OBJECT (sink, "new base time: %" GST_TIME_FORMAT,
            GST_TIME_ARGS(element->base_time));
      }
      GST_OBJECT_UNLOCK (sink);
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      sink->need_discont = TRUE;
      break;
    default:
      break;
  }

  return ret;
}

static void
gst_flugdlsink_do_mute (GstFluGdlSink *sink)
{
  GstBaseSink *bsink = GST_BASE_SINK (sink);
  gdl_ret_t st;

  GST_PAD_PREROLL_LOCK (bsink->sinkpad);
  if (bsink->clock_id) {
    GST_DEBUG_OBJECT (sink, "unschedule duration active wait");
    gst_clock_id_unschedule (bsink->clock_id);
  }
  if (G_LIKELY (sink->clear_surface.id != GDL_SURFACE_INVALID)) {
    GST_DEBUG_OBJECT (sink, "render clear surface");
    st = gdl_flip (sink->video_plane, sink->clear_surface.id,
        GDL_FLIP_SYNC);
    if (st != GDL_SUCCESS) {
      GST_ERROR_OBJECT (sink, "gdl_flip failed %d", st);
    }
  }
  GST_PAD_PREROLL_UNLOCK (bsink->sinkpad);
}

static void
gst_flugdlsink_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  GstFluGdlSink *sink = GST_FLUGDLSINK (object);

  switch (prop_id) {
    case PROP_TVMODE:
      g_value_set_enum (value, sink->tvmode);
      break;
    case PROP_GDL_PLANE:
      g_value_set_enum (value, sink->video_plane);
      break;
    case PROP_MUTE:
      g_value_set_boolean (value, sink->mute);
      break;
    case PROP_XPSM:
      g_value_set_boolean (value, sink->xpsm);
      break;
    case PROP_SCALE_MODE:
      g_value_set_enum (value, sink->scale_mode);
      break;
    case PROP_RECTANGLE:
      g_value_transform (&sink->rectangle, value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_flugdlsink_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  GstBaseSink *bsink = GST_BASE_SINK (object);
  GstFluGdlSink *sink = GST_FLUGDLSINK (object);

  switch (prop_id) {
    case PROP_TVMODE:
      sink->tvmode = g_value_get_enum (value);
      break;
    case PROP_GDL_PLANE:
      GST_OBJECT_LOCK (sink);
      reconfig_plane (sink, g_value_get_enum (value));
      GST_OBJECT_UNLOCK (sink);
      break;
    case PROP_MUTE:
      {
        gboolean mute = g_value_get_boolean (value);

        GST_DEBUG_OBJECT (sink, "set mute property to %s",
            (mute ? "TRUE" : "FALSE"));

        GST_OBJECT_LOCK (bsink);
        sink->mute = mute;
        GST_OBJECT_UNLOCK (bsink);

        if (mute)
          gst_flugdlsink_do_mute (sink);
        break;
      }
    case PROP_XPSM:
      {
        GST_OBJECT_LOCK (bsink);
        sink->xpsm = g_value_get_boolean (value);;
        GST_OBJECT_UNLOCK (bsink);        
        break;
      }
    case PROP_SCALE_MODE:
      sink->scale_mode = g_value_get_enum (value);
      /* If scale-mode has been changed during playback, reconfigure driver */
      if (sink->setup && GST_VIDEO_SINK_WIDTH (sink) &&
              GST_VIDEO_SINK_HEIGHT (sink)) {
        GST_OBJECT_LOCK (sink);
        config_upscale (sink, sink->video_plane, GST_VIDEO_SINK_WIDTH (sink),
            GST_VIDEO_SINK_HEIGHT (sink));
        GST_OBJECT_UNLOCK (sink);            
      }
      break;
    case PROP_RECTANGLE:
      if (!g_value_transform (value, &sink->rectangle)) {
        g_warning ("Could not transform string to rectangle");
        gst_value_set_rectangle (&sink->rectangle, 0, 0, 0, 0);
      }
      GST_DEBUG_OBJECT (sink, "set destination rectangle to %d,%d,%d,%d",
          gst_value_get_rectangle_x (&sink->rectangle),
          gst_value_get_rectangle_y (&sink->rectangle),
          gst_value_get_rectangle_width (&sink->rectangle),
          gst_value_get_rectangle_height (&sink->rectangle));
      if (sink->caps_available)
        config_upscale (sink, sink->video_plane, GST_VIDEO_SINK_WIDTH (sink),
            GST_VIDEO_SINK_HEIGHT (sink));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_flugdlsink_finalize (GObject * object)
{
  GstFluGdlSink *sink = GST_FLUGDLSINK (object);

  g_value_unset (&sink->rectangle);

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_flugdlsink_base_init (gpointer g_class)
{
  GstElementClass *element_class = GST_ELEMENT_CLASS (g_class);

  gst_element_class_set_details (element_class, &flugdlsink_details);

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&flugdlsink_factory));
}

static void
gst_flugdlsink_class_init (GstFluGdlSinkClass * klass)
{
  GstBaseSinkClass *basesink_class = GST_BASE_SINK_CLASS (klass);
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);
  GObjectClass *gobject_class = G_OBJECT_CLASS (klass);

  gobject_class->set_property = gst_flugdlsink_set_property;
  gobject_class->get_property = gst_flugdlsink_get_property;

  g_object_class_install_property (gobject_class,
      PROP_TVMODE,
      g_param_spec_enum ("tvmode",
          "Television mode",
          "Define the television mode",
          GST_TYPE_FLU_GDL_SINK_TVMODE, DEFAULT_TVMODE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class,
      PROP_GDL_PLANE,
      g_param_spec_enum ("gdl-plane",
          "UPP used for rendering",
          "Define the Universal Pixel Plane used in the GDL layer",
          GST_TYPE_FLU_GDL_SINK_PLANE, DEFAULT_PLANE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class,
      PROP_MUTE,
      g_param_spec_boolean ("mute", "Mute",
          "Remove current rendered buffer and drop all following ones",
          DEFAULT_MUTE, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class,
      PROP_XPSM,
      g_param_spec_boolean ("xpsm", "XPSM",
          "Correct timming to match XPSM behaviour",
          DEFAULT_XPSM, G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class,
      PROP_SCALE_MODE,
      g_param_spec_enum ("scale-mode",
          "Scale mode",
          "Define the mode used by the video scaler",
          GST_TYPE_FLU_GDL_SINK_SCALE_MODE, DEFAULT_SCALE_MODE,
          G_PARAM_READWRITE));

  g_object_class_install_property (gobject_class,
      PROP_RECTANGLE,
      g_param_spec_string ("rectangle",
          "Destination rectangle",
          "The destination rectangle, (0,0,0,0) full screen", 
          DEFAULT_RECTANGLE,
          G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS));

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_flugdlsink_change_state);

  gobject_class->finalize = GST_DEBUG_FUNCPTR (gst_flugdlsink_finalize);
  basesink_class->start = GST_DEBUG_FUNCPTR (gst_flugdlsink_start);
  basesink_class->stop = GST_DEBUG_FUNCPTR (gst_flugdlsink_stop);
  basesink_class->render = GST_DEBUG_FUNCPTR (gst_flugdlsink_render);
  basesink_class->event = GST_DEBUG_FUNCPTR (gst_flugdlsink_event);
  basesink_class->set_caps = GST_DEBUG_FUNCPTR (gst_flugdlsink_set_caps);
  basesink_class->get_caps = GST_DEBUG_FUNCPTR (gst_flugdlsink_get_caps);
}

static void
gst_flugdlsink_init (GstFluGdlSink * sink, GstFluGdlSinkClass * klass)
{
  sink->caps_available = NULL;
  sink->par_n = 0;
  sink->par_d = 0;

  sink->tvmode = DEFAULT_TVMODE;
  sink->gamma = GDL_GAMMA_LINEAR;       /* GDL_GAMMA_2_2 GDL_GAMMA_2_8 */
  sink->video_plane = DEFAULT_PLANE;
  sink->cc_plane = GDL_PLANE_ID_UNDEFINED;
  sink->display_id = DEFAULT_DISPLAY_ID;
  sink->mute = DEFAULT_MUTE;
  sink->xpsm = DEFAULT_XPSM;
  sink->need_discont = TRUE;
  sink->scale_mode = DEFAULT_SCALE_MODE;
  g_value_init (&sink->rectangle, GST_TYPE_RECTANGLE);
  gst_value_set_rectangle (&sink->rectangle, 0, 0, 0, 0);

  sink->brightness = 0;
  sink->contrast = 0;
  sink->hue = 0;
  sink->aspect_ratio = 0;
  sink->setup = FALSE;

  memset (&sink->palette, 0, sizeof (gdl_palette_t));
  sink->palette.length = 256;

  reset (sink);
  gst_base_sink_set_qos_enabled (GST_BASE_SINK (sink), TRUE);
  gst_base_sink_set_async_enabled (GST_BASE_SINK (sink), FALSE);
  gst_base_sink_set_max_lateness (GST_BASE_SINK (sink), 20000000);
}

/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and pad templates
 * register the features
 */
static gboolean
plugin_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (gst_debug_flugdlsink, "flugdlsink", 0,
      "Fluendo GDL sink element");

  if (!gst_element_register (plugin, "flugdlsink", GST_RANK_MARGINAL,
          GST_TYPE_FLUGDLSINK))
    return FALSE;

  gst_value_rectangle_register ();

  /* plugin initialisation succeeded */
  return TRUE;
}

/* this is the structure that gst-register looks for
 * so keep the name plugin_desc, or you cannot get your plug-in registered */
GST_PLUGIN_DEFINE (GST_VERSION_MAJOR, GST_VERSION_MINOR,
    "flugdlsink", "Fluendo GDL sink plugin",
    plugin_init, VERSION, "BSD", PACKAGE_NAME,
    "http://www.fluendo.com");
